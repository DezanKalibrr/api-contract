package main

import (
	"fmt"
	"git.kalibrr.dev/kalibrr/migrations/exodus-helper/api-contract/internal"
	"github.com/labstack/echo/v4"
	"os"
)

// @title Kalibrr API Contract
// @version 1.0
// @description Kalibrr API Contract and Documentation.
// @termsOfService http://swagger.io/terms/

// @contact.name Kalibrr Exodus Dev
// @contact.url http://www.google.com
// @contact.email dezan@kalibrr.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api/v1
// @securityDefinitions.basic Authorization
func main() {
	e := echo.New()
	customRender := &internal.CustomRenderer{
		Templates: internal.CustomParsesTemplate(),
	}
	e.Renderer = customRender

	e.File("/", "public/index.html")

	inMemoryDB := internal.NewInMemoryDB()
	internal.SetService(&inMemoryDB)

	internalRouter := internal.NewRouter(&inMemoryDB)
	internalRouter.ServeStaticFile(e)
	internalRouter.Router(e)

	setupPort := os.Getenv("PORT")
	if setupPort == "" {
		setupPort = ":5501"
	} else {
		setupPort = fmt.Sprintf(":%s", os.Getenv("PORT"))
	}

	e.Logger.Fatal(e.Start(setupPort))
}
