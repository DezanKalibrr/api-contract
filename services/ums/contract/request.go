package contract

type LoginRequest struct {
	Identity string `json:"identity" example:"dezan@kalibrr.com"`
	Password string `json:"password" example:"kalibrr999"`
}
