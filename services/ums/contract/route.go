package contract

import "github.com/labstack/echo/v4"

type UmsRoute struct {
}

func NewUmsRoute() UmsRoute {
	return UmsRoute{}
}

func (u *UmsRoute) Route(e *echo.Group) {
	e.POST("/login", u.login)
	e.POST("/logout", u.logout)
	e.POST("/signup", u.signup)
	e.POST("/forgot-password", u.forgetPassword)
}

// Login : Login to Kalibrr Platform
// @Summary Login Service
// @Description This endpoint for User Login
// @Tags User Management Service
// @Accept  json
// @Produce  json
// @Param payload body LoginRequest true "Payload"
// @Success 200 {object} LoginResponse "desc"
// @Router /ums/login [post]
// @Security Authorization
func (u *UmsRoute) login(ctx echo.Context) error {
	return ctx.JSON(200, LoginResponse{
		Claims: "token12345678",
	})
}

// Logout : Logout to Kalibrr Platforms
// @Summary Logout Service
// @Description This endpoint for User Logout
// @Tags User Management Service
// @Accept  json
// @Produce  json
// @Param payload body LoginRequest true "Payload"
// @Success 200 {object} LoginResponse "desc"
// @Router /ums/logout [post]
// @Security Authorization
func (u *UmsRoute) logout(ctx echo.Context) error {
	return ctx.JSON(200, LoginResponse{
		Claims: "token12345678",
	})
}

// Logout : Signup to Kalibrr Platforms
// @Summary Signup Service
// @Description This endpoint for User Signup
// @Tags User Management Service
// @Accept  json
// @Produce  json
// @Param payload body LoginRequest true "Payload"
// @Success 200 {object} LoginResponse "desc"
// @Router /ums/signup [post]
// @Security Authorization
func (u *UmsRoute) signup(ctx echo.Context) error {
	return ctx.JSON(200, LoginResponse{
		Claims: "token12345678",
	})
}

// Logout : Forgot Password to Kalibrr Platforms
// @Summary Forgot Password Service
// @Description This endpoint for User Forgot Password
// @Tags User Management Service
// @Accept  json
// @Produce  json
// @Param payload body LoginRequest true "Payload"
// @Success 200 {object} LoginResponse "desc"
// @Router /ums/forgot-password [post]
// @Security Authorization
func (u *UmsRoute) forgetPassword(ctx echo.Context) error {
	return ctx.JSON(200, LoginResponse{
		Claims: "token12345678",
	})
}
