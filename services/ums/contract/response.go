package contract

type LoginResponse struct {
	Claims string `json:"claims" example:"jwt token here"`
}

type SignUpResponse struct {
	Identity string `json:"identity" example:"dezan@kalibrr.com"`
	Password string `json:"password" example:"kalibrr999"`
}
