package contract

import "github.com/labstack/echo/v4"

type JobseekerRoute struct {
}

func NewJobseekerRoute() JobseekerRoute {
	return JobseekerRoute{}
}

func (u *JobseekerRoute) Route(e *echo.Group) {
	e.PUT("/profile", u.login)
}

// Login : Login to Kalibrr Platform
// @Summary Login Service
// @Description This endpoint for User Login
// @Tags Jobseeker Service
// @Accept  json
// @Produce  json
// @Param payload body UpdateProfileRequest true "Payload"
// @Success 200 {object} UpdateProfileResponse "desc"
// @Router /api/v1/jobseeker/profile [PUT]
func (u *JobseekerRoute) login(ctx echo.Context) error {
	return ctx.JSON(200, UpdateProfileResponse{
		Name:    "Dezan",
		Address: "Jakarta",
	})
}
