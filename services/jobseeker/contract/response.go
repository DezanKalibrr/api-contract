package contract

type UpdateProfileResponse struct {
	Name    string `json:"name" example:"dezan"`
	Address string `json:"address" example:"jakarta"`
}
