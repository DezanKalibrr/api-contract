package main

import (
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
)

func main() {
	var folders []string

	files, err := ioutil.ReadDir("./services")
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		folders = append(folders, f.Name())
	}

	err = generateSwaggo(folders)
	if err != nil {
		panic(err)
	}

	log.Println("Swaggo Generated Successfully")
}

func generateSwaggo(folders []string) error {
	build := buildCommand(folders)
	cmd := exec.Command("cd", "..")
	_, err := cmd.CombinedOutput()

	for _, val := range build {
		split := strings.Split(val, " ")
		err = execCommand(split...)
		if err != nil {
			log.Println("Please Install Swaggo By Running This Script: go install github.com/swaggo/swag/cmd/swag@latest")
			break
		}
	}

	return err
}

func buildCommand(folders []string) []string {
	var commands []string
	script := "init -p snakecase -o ./gen/$name --exclude $none"

	for i, value := range folders {
		build := strings.Replace(script, "$name", value, 1)
		var none string

		for x, val := range folders {
			if i == x {
				continue
			}
			none += "./services/" + val + ","
		}
		none = strings.TrimSuffix(none, ",")
		completeCommand := strings.Replace(build, "$none", none, 1)
		commands = append(commands, completeCommand)
		none = ""
	}
	return commands
}

func execCommand(command ...string) error {
	cmd := exec.Command("swag", command...)
	_, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}

	return nil
}
