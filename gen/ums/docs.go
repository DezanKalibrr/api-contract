// Package ums GENERATED BY SWAG; DO NOT EDIT
// This file was generated by swaggo/swag
package ums

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "termsOfService": "http://swagger.io/terms/",
        "contact": {
            "name": "Kalibrr Exodus Dev",
            "url": "http://www.google.com",
            "email": "dezan@kalibrr.com"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/ums/forgot-password": {
            "post": {
                "security": [
                    {
                        "Authorization": []
                    }
                ],
                "description": "This endpoint for User Forgot Password",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "User Management Service"
                ],
                "summary": "Forgot Password Service",
                "parameters": [
                    {
                        "description": "Payload",
                        "name": "payload",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/contract.LoginRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "desc",
                        "schema": {
                            "$ref": "#/definitions/contract.LoginResponse"
                        }
                    }
                }
            }
        },
        "/ums/login": {
            "post": {
                "security": [
                    {
                        "Authorization": []
                    }
                ],
                "description": "This endpoint for User Login",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "User Management Service"
                ],
                "summary": "Login Service",
                "parameters": [
                    {
                        "description": "Payload",
                        "name": "payload",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/contract.LoginRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "desc",
                        "schema": {
                            "$ref": "#/definitions/contract.LoginResponse"
                        }
                    }
                }
            }
        },
        "/ums/logout": {
            "post": {
                "security": [
                    {
                        "Authorization": []
                    }
                ],
                "description": "This endpoint for User Logout",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "User Management Service"
                ],
                "summary": "Logout Service",
                "parameters": [
                    {
                        "description": "Payload",
                        "name": "payload",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/contract.LoginRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "desc",
                        "schema": {
                            "$ref": "#/definitions/contract.LoginResponse"
                        }
                    }
                }
            }
        },
        "/ums/signup": {
            "post": {
                "security": [
                    {
                        "Authorization": []
                    }
                ],
                "description": "This endpoint for User Signup",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "User Management Service"
                ],
                "summary": "Signup Service",
                "parameters": [
                    {
                        "description": "Payload",
                        "name": "payload",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/contract.LoginRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "desc",
                        "schema": {
                            "$ref": "#/definitions/contract.LoginResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "contract.LoginRequest": {
            "type": "object",
            "properties": {
                "identity": {
                    "type": "string",
                    "example": "dezan@kalibrr.com"
                },
                "password": {
                    "type": "string",
                    "example": "kalibrr999"
                }
            }
        },
        "contract.LoginResponse": {
            "type": "object",
            "properties": {
                "claims": {
                    "type": "string",
                    "example": "jwt token here"
                }
            }
        }
    },
    "securityDefinitions": {
        "Authorization": {
            "type": "basic"
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "",
	BasePath:         "/api/v1",
	Schemes:          []string{},
	Title:            "Kalibrr API Contract",
	Description:      "Kalibrr API Contract and Documentation.",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
