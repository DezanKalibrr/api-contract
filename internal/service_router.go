package internal

import (
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
)

type Router struct {
	db *InMemoryDB
}

func NewRouter(db *InMemoryDB) *Router {
	return &Router{
		db: db,
	}
}

func (r *Router) ServeStaticFile(e *echo.Echo) {
	e.Static("/api-version/", "public/api-versions")
	e.Static("/api-version/gen", "gen")
}

func (r *Router) Router(e *echo.Echo) {
	e.GET("/", r.index)
	e.GET("/api-version/:serviceAlias", r.versioning)
}

func (r *Router) index(ctx echo.Context) error {
	err := ctx.Render(http.StatusOK, "index.html", map[string]interface{}{
		"data": r.db.ToMap(),
	})

	if err != nil {
		log.Println(err)
	}
	return err
}

func (r *Router) versioning(ctx echo.Context) error {
	serviceAlias := ctx.Param("serviceAlias")
	serviceName, err := r.db.Get(serviceAlias)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Service not found",
		})
	}

	err = ctx.Render(http.StatusOK, "version_template.html", map[string]interface{}{
		"service":      serviceAlias,
		"service_name": serviceName,
	})

	if err != nil {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Service not found",
		})
	}
	return err
}
