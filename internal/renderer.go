package internal

import (
	"github.com/labstack/echo/v4"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type CustomRenderer struct {
	Templates *template.Template
}

func (t *CustomRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.Templates.ExecuteTemplate(w, name, data)
}

func CustomParsesTemplate() *template.Template {
	templ := template.New("")
	err := filepath.Walk("./public", func(path string, info os.FileInfo, err error) error {
		if strings.Contains(path, ".html") {
			_, err = templ.ParseFiles(path)
			if err != nil {
				log.Println(err)
			}
		}

		return err
	})

	if err != nil {
		panic(err)
	}

	return templ
}
