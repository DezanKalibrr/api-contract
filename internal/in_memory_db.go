package internal

import "fmt"

type Item struct {
	ServiceName string
}

type InMemoryDB map[string]Item

func (db InMemoryDB) Get(key string) (Item, error) {
	item, ok := db[key]
	if !ok {
		return Item{}, fmt.Errorf("key %s not found", key)
	}
	return item, nil
}

func (db InMemoryDB) Set(key string, item Item) {
	db[key] = item
}

func (db InMemoryDB) Delete(key string) {
	delete(db, key)
}

func (db InMemoryDB) ToMap() map[string]string {
	m := make(map[string]string)
	for key, value := range db {
		m[key] = value.ServiceName
	}

	return m
}

func NewInMemoryDB() InMemoryDB {
	return make(InMemoryDB)
}
