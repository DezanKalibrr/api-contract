package internal

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func SetService(db *InMemoryDB) {
	files, err := ioutil.ReadDir("./services")
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		finalName := ""
		finalName = fmt.Sprintf("%s%s", strings.ToUpper(string(f.Name()[0])), strings.ToLower(string(f.Name()[1:])))

		db.Set(f.Name(), Item{ServiceName: finalName})
	}
}
